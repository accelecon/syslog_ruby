module SyslogRuby
  class Formatter

    attr_accessor :priority, :version, :timestamp, :hostname, :app_name,
      :proc_id, :msg_id, :separator, :msg

    def initialize(attrs = {})
      attrs.each do |key, value|
        instance_variable_set("@#{key}", value)
      end

      yield(self) if block_given?
    end

    def to_s
      string = "<#{priority}>#{version} #{timestamp} "
      string << [hostname, app_name, proc_id, msg_id].compact.join(' ')
      string << separator
      string << msg
      string << "\n"
      string
    end
  end
end
